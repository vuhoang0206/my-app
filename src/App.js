import './App.css';
import HomeRoute from './components/Router/HomeRoute';
import { BrowserRouter as Router } from "react-router-dom";


function App() {
  return (
    <Router>
      <HomeRoute />
    </Router>

  );
}

export default App;
