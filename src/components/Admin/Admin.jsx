import React, { Component } from 'react'
import Nav from './Nav'
import RouterAdmin from './RouterAdmin/RouterAdmin'
import {
    BrowserRouter as Router
} from "react-router-dom";

export default class Admin extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Nav />
                    <hr />
                    <RouterAdmin />
                </div>
            </Router>
        )
    }
}
