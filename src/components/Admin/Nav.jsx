import React, { Component } from 'react'
import { Dropdown } from 'react-bootstrap'
import { NavLink } from "react-router-dom";

export default class Nav extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-md bg-dark navbar-dark">
                <a className="navbar-brand" href="/quanly">Admin</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/dashboard">Dashboard</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/qlban">QL Bàn</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/qlthucdon">QL Thực Đơn</NavLink>
                        </li>
                        {/* <li className="nav-item">
                            <NavLink className="nav-link" to="/qlhoadon">QL Hóa Đơn</NavLink>
                        </li> */}
                    </ul>
                    <ul className="nav navbar-nav navbar-right">
                        <Dropdown>
                            <Dropdown.Toggle variant="success" id="dropdown-basic">
                                Quan ly</Dropdown.Toggle>
                            <Dropdown.Menu>
                                <Dropdown.Item href="/">Trang chủ</Dropdown.Item>
                                <Dropdown.Item href="/login">Đăng xuất</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </ul>
                </div>
            </nav>
        )
    }
}
