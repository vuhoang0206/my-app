import React, { Component } from 'react'

export default class QLBan extends Component {
    render() {
        return (
            <div class="container">
                <h1>Quản lý bàn</h1>
                <br />
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên bàn</th>
                            <th>Trạng thái</th>
                            <th>
                                <button type="button" class="btn btn-primary">Thêm</button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Ban 1</td>
                            <td>Active</td>
                            <td>
                            <button type="button" class="btn btn-success">Sửa</button>
                            <button type="button" class="btn btn-danger">Xóa</button>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Ban 2</td>
                            <td>Active</td>
                            <td>
                            <button type="button" class="btn btn-success">Sửa</button>
                            <button type="button" class="btn btn-danger">Xóa</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}
