import React, { Component } from 'react'

export default class QLHoaDon extends Component {
    render() {
        return (
            <div class="container">
                <h1>Quản lý hóa đơn</h1>
                <br />
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên bàn</th>
                            <th>Mã hóa đơn</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Bàn 1</td>
                            <td>HD001</td>
                            <td>
                            <button type="button" class="btn btn-success">Chi tiết</button>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        )
    }
}
