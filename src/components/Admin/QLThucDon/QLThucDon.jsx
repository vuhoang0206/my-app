import React, { Component } from 'react'

export default class QLThucDon extends Component {
    render() {
        return (
            <div class="container">
                <h1>Quản lý thực đơn</h1>
                <br />
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên thực đơn</th>
                            <th>Loại thực đơn</th>
                            <th>Giá</th>
                            <th>
                                <button type="button" class="btn btn-primary">Thêm</button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Thực đơn 1</td>
                            <td>Đồ ăn</td>
                            <td>10000</td>
                            <td>
                            <button type="button" class="btn btn-success">Sửa</button>
                            <button type="button" class="btn btn-danger">Xóa</button>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Thực đơn 2</td>
                            <td>Đồ uống</td>
                            <td>20000</td>
                            <td>
                            <button type="button" class="btn btn-success">Sửa</button>
                            <button type="button" class="btn btn-danger">Xóa</button>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        )
    }
}
