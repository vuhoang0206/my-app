import React from 'react'
import { Switch, Route } from "react-router-dom";
import Dashboard from '../Dashboard/Dashboard'
import QLBan from '../QLBan/QLBan'
import QLThucDon from '../QLThucDon/QLThucDon'
import QLHoaDon from '../QLHoaDon/QLHoaDon'


function RouterAdmin() {
    return (
        <Switch>
            <Route exact path="/dashboard">
                <Dashboard />
            </Route>
            <Route path="/qlban">
                <QLBan />
            </Route>
            <Route path="/qlthucdon">
                <QLThucDon />
            </Route>
            <Route path="/qlhoadon">
                <QLHoaDon />
            </Route>
        </Switch>
    )
};

export default RouterAdmin;