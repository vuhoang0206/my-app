import React, { Component } from 'react'
import LeftHome from './LeftHome/LeftHome'
import RightHome from './RightHome/RightHome'

export default class Home extends Component {
    render() {
        return (
            <div className="container-fluid ">
                <div className="row ">
                    <div className=" col-md-7 col-xl-7 col-ms-7 col-lg-7">
                        <LeftHome />
                    </div>
                    <div className="col-md-5 col-xl-5 col-ms-5 col-lg-5">
                        <RightHome />
                        <div>Ten ban</div>
                    </div>
                </div>
            </div>
        )
    }
}
