import React, { Component } from 'react'
import {Tabs, Tab} from 'react-bootstrap'
import Table from './Table/Table'
import Menu from './Menu/Menu'

export default class LeftHome extends Component {
    render() {
        return (
            <Tabs defaultActiveKey="table" transition={false} id="noanim-tab-example">
                <Tab eventKey="table" title="Bàn">
                    <Table />
                </Tab>
                <Tab eventKey="menu" title="Thực đơn">
                    <Menu />
                </Tab>
            </Tabs>
        )
    }
}
