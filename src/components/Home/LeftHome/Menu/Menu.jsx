import React, { Component } from 'react'
import food from './food.jpg'
import { Card } from 'react-bootstrap'
import { Tabs, Tab } from 'react-bootstrap'



export default class Menu extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div>
                    <Tabs defaultActiveKey="all" transition={false} id="noanim-tab-example">
                        <Tab eventKey="all" title="Tat ca">
                            abc
                        </Tab>
                        <Tab eventKey="doan" title="Do an">
                            Do an
                        </Tab>
                        <Tab eventKey="douong" title="Do uong">
                            Do uong
                        </Tab>
                        <Tab eventKey="khac" title="Khac">
                            Khac
                        </Tab>
                    </Tabs>
                </div>
                <div className="row">
                    <div className="col-md-3 col-xl-3 col-ms-3 col-lg-3">
                        <button type="button" className="b">
                            <Card>
                                <Card.Img variant="top" src={food} />
                                <Card.Body>
                                    <Card.Title>Card Title</Card.Title>
                                    <Card.Text>100.000 VND</Card.Text>
                                </Card.Body>
                            </Card>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}
