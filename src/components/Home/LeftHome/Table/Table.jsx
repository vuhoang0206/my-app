import React, { Component } from 'react'

export default class Table extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-2 col-xl-2 col-ms-2 col-lg-2">
                        <button type="button" className="a btn-primary">Ban 1</button>
                    </div>
                    <div className="col-md-2 col-xl-2 col-ms-2 col-lg-2">
                        <button type="button" className="a btn-primary">Ban 2</button>
                    </div>
                    <div className="col-md-2 col-xl-2 col-ms-2 col-lg-2">
                        <button type="button" className="a btn-primary">Ban 3</button>
                    </div>
                    <div className="col-md-2 col-xl-2 col-ms-2 col-lg-2">
                        <button type="button" className="a btn-primary">Ban 4</button>
                    </div>
                    <div className="col-md-2 col-xl-2 col-ms-2 col-lg-2">
                        <button type="button" className="a btn-primary">Ban 5</button>
                    </div>

                </div>
            </div>
        )
    }
}
