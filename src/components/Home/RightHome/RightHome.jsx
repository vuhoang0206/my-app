import React, { Component } from 'react'
import { Dropdown } from 'react-bootstrap'

export default class RightHome extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-sm bg-light navbar-light">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <i className='fas'>Hóa đơn</i>
                        </li>
                    </ul>
                    <ul className="nav navbar-nav">
                        <li className="nav-item">
                            <Dropdown>
                                <Dropdown.Toggle variant="success" id="dropdown-basic">
                                    Trang chủ</Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item href="/quanly">Quản lý</Dropdown.Item>
                                    <Dropdown.Item href="/login">Đăng xuất</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </li>
                    </ul>
                </nav>

            </div>
        )
    }
}
