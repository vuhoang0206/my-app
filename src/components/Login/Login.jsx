import React, { useState } from 'react';
import "./Login.style.css";
import {Form, Button} from "react-bootstrap";

const  Login = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const handleChangeUserName = (e) => {
        e.preventDefault();
        setUsername(e.target.value);
    }

    const handleChangePassword = (e) => {
        e.preventDefault();
        setPassword(e.target.value);
    }

    const handleLogin = () => {
        console.log("looo")
    }   
    

    return(
        <div className="auth-login__container">
                <div className="box">
                    <Form onSubmit={handleLogin}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" value={username} onChange={handleChangeUserName} />
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" value={password} onChange={handleChangePassword} />
                        </Form.Group>
                        <Form.Group controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </div>
            </div>
    )
};
export default Login;