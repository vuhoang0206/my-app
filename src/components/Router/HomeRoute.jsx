import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom';
import Home from '../Home/Home';
import Login from '../Login/Login';
import Admin from '../Admin/Admin'

class HomeRoute extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/" >
                        <Home />
                    </Route>
                    <Route path="/login" >
                        <Login />
                    </Route>
                    <Route path="/quanly" >
                        <Admin />
                    </Route>
                    <Route>
                        <Home />
                    </Route>
                </Switch>
            </div>

        )
    }
}
export default HomeRoute